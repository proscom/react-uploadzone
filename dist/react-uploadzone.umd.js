(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react'), require('react-dropzone')) :
    typeof define === 'function' && define.amd ? define(['exports', 'react', 'react-dropzone'], factory) :
    (factory((global.reactUploadzone = {}),global.React,global.Dropzone));
}(this, (function (exports,React,Dropzone) { 'use strict';

    React = React && React.hasOwnProperty('default') ? React['default'] : React;
    Dropzone = Dropzone && Dropzone.hasOwnProperty('default') ? Dropzone['default'] : Dropzone;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    (function (RejectionReason) {
        RejectionReason["MIME_TYPE"] = "MIME_TYPE";
        RejectionReason["MAX_FILE_SIZE"] = "MAX_FILE_SIZE";
    })(exports.RejectionReason || (exports.RejectionReason = {}));
    var FileUploadError = /** @class */ (function () {
        function FileUploadError(file, message) {
            this.file = file;
            this.message = message;
        }
        return FileUploadError;
    }());
    var FileRejectedError = /** @class */ (function () {
        function FileRejectedError(file, reason) {
            this.file = file;
            this.reason = reason;
        }
        return FileRejectedError;
    }());
    var MaxFilesError = /** @class */ (function () {
        function MaxFilesError() {
        }
        return MaxFilesError;
    }());
    // End Error types
    function compareObjectArrays(oldFiles, newFiles) {
        var oldMap = {};
        var newMap = {};
        oldFiles.forEach(function (file) { return (oldMap[file.id] = file); });
        newFiles.forEach(function (file) { return (newMap[file.id] = file); });
        var added = newFiles.filter(function (file) { return !oldMap[file.id]; });
        var removed = oldFiles.filter(function (file) { return !newMap[file.id]; });
        var same = oldFiles.filter(function (file) { return newMap[file.id]; });
        return {
            added: added,
            removed: removed,
            same: same,
            changed: added.length > 0 || removed.length > 0
        };
    }
    var UploadZone = /** @class */ (function (_super) {
        __extends(UploadZone, _super);
        function UploadZone() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.state = {
                files: []
            };
            _this.fileIdSequence = 0;
            _this.filesUploading = [];
            _this.onDrop = function (acceptedFiles, rejectedFiles) {
                if (rejectedFiles && rejectedFiles.length) {
                    rejectedFiles.forEach(function (file) {
                        _this.onError(new FileRejectedError(file, exports.RejectionReason.MIME_TYPE));
                    });
                }
                if (_this.props.maxFiles &&
                    _this.state.files.length + acceptedFiles.length > _this.props.maxFiles) {
                    _this.onError(new MaxFilesError());
                    return;
                }
                var passed = [];
                acceptedFiles.forEach(function (file) {
                    if (_this.props.maxFileSize && file.size > _this.props.maxFileSize) {
                        _this.onError(new FileRejectedError(file, exports.RejectionReason.MAX_FILE_SIZE));
                        return;
                    }
                    passed.push({
                        id: _this.fileIdSequence++,
                        nativeFile: file
                    });
                });
                _this.setState({
                    files: _this.state.files.concat(passed)
                });
            };
            _this.removeFile = function (fileId) {
                _this.setState({
                    files: _this.state.files.filter(function (f) { return f.id !== fileId; })
                });
            };
            _this.cancelUpload = function (fileId) {
                var file = _this.state.files.find(function (f) { return f.id === fileId; });
                if (file && file.request) {
                    file.request.abort();
                }
                _this.removeFile(fileId);
            };
            return _this;
        }
        UploadZone.prototype.componentDidUpdate = function (oldProps, oldState) {
            if (oldState.files !== this.state.files) {
                this.props.onChange && this.props.onChange(this.state.files);
                var filesUploading = this.state.files.filter(function (f) { return f.isUploading; });
                var changes = compareObjectArrays(this.filesUploading, filesUploading);
                if (changes.changed) {
                    this.filesUploading = filesUploading;
                    if (filesUploading.length === 0 && changes.removed.length > 0) {
                        this.props.onFilesUploaded &&
                            this.props.onFilesUploaded(this.state.files);
                    }
                    if (changes.added.length > 0) {
                        this.props.onFilesUploading &&
                            this.props.onFilesUploading(changes.added, filesUploading);
                    }
                }
                this.uploadNewFiles(this.state.files);
            }
        };
        UploadZone.prototype.getFiles = function () {
            return this.state.files;
        };
        UploadZone.prototype.uploadNewFiles = function (files) {
            var _this = this;
            var isChanged = false;
            var newFiles = files.map(function (file) {
                if (!file.isUploadAttempted) {
                    isChanged = true;
                    var request = _this.sendRequest(file);
                    return __assign({}, file, { request: request, isUploading: true, isUploadAttempted: true, progress: 0 });
                }
                else {
                    return file;
                }
            });
            if (isChanged) {
                this.setState({ files: newFiles });
            }
        };
        UploadZone.prototype.sendRequest = function (file) {
            var _this = this;
            return this.props.uploadFile({
                file: file,
                onSuccess: function (data) {
                    _this.updateFile(file.id, function () { return ({
                        response: data,
                        isUploading: false,
                        isUploaded: true
                    }); });
                },
                onError: function (error) {
                    _this.updateFile(file.id, function () { return ({
                        isUploading: false,
                        error: error
                    }); });
                    _this.onError(new FileUploadError(file, error));
                },
                onProgress: function (loaded, total) {
                    var progress = Math.round((loaded / total) * 100);
                    _this.updateFile(file.id, function () { return ({ progress: progress }); });
                }
            });
        };
        UploadZone.prototype.updateFile = function (id, updater) {
            this.setState(function (state) {
                var isChanged = false;
                var files = state.files.map(function (f) {
                    if (f.id === id) {
                        isChanged = true;
                        return __assign({}, f, updater(f));
                    }
                    else {
                        return f;
                    }
                });
                if (!isChanged) {
                    console.warn("File with id=" + id + " not found. This may be due to the uploading request not properly aborted."); // `
                }
                return { files: files };
            });
        };
        UploadZone.prototype.onError = function (error) {
            this.props.onError && this.props.onError(error);
        };
        UploadZone.prototype.render = function () {
            var _this = this;
            return (React.createElement(Dropzone, { accept: this.props.accept, preventDropOnDocument: this.props.preventDropOnDocument, disabled: this.props.disabled, multiple: true, onDrop: this.onDrop }, function (dropzoneProps) {
                return _this.props.children(__assign({}, dropzoneProps, { files: _this.state.files, removeFile: _this.removeFile, cancelUpload: _this.cancelUpload }));
            }));
        };
        return UploadZone;
    }(React.Component));

    exports.FileUploadError = FileUploadError;
    exports.FileRejectedError = FileRejectedError;
    exports.MaxFilesError = MaxFilesError;
    exports.UploadZone = UploadZone;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=react-uploadzone.umd.js.map
