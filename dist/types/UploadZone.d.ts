import React from 'react';
import { DropzoneRenderArgs } from 'react-dropzone';
export interface IProps {
    accept?: string;
    preventDropOnDocument?: boolean;
    disabled?: boolean;
    maxFiles?: number;
    maxFileSize?: number;
    uploadFile: FileUploader;
    onChange?: (files: IFile[]) => void;
    onError?: (e: UploadZoneError) => void;
    onFilesUploaded?: (files: IFile[]) => void;
    onFilesUploading?: (added: IFile[], uploading: IFile[]) => void;
    children: (props: DropzoneRenderArgs & IUploadZoneRenderArgs) => JSX.Element;
}
export interface IRequest {
    abort: () => void;
}
export declare type FileUploader = (args: {
    file: IFile;
    onSuccess: (data: any) => void;
    onError: (error: string) => void;
    onProgress: (loaded: number, total: number) => void;
}) => IRequest;
export interface IUploadZoneRenderArgs {
    files: IFile[];
    removeFile: (id: number) => void;
    cancelUpload: (id: number) => void;
}
export interface IFile {
    id: number;
    nativeFile: File;
    request?: IRequest;
    progress?: number;
    response?: any;
    isUploadAttempted?: boolean;
    isUploading?: boolean;
    isUploaded?: boolean;
    error?: string;
}
export interface IState {
    files: IFile[];
}
export declare enum RejectionReason {
    MIME_TYPE = "MIME_TYPE",
    MAX_FILE_SIZE = "MAX_FILE_SIZE"
}
export declare class FileUploadError {
    file: IFile;
    message: string;
    constructor(file: IFile, message: string);
}
export declare class FileRejectedError {
    file: File;
    reason: RejectionReason;
    constructor(file: File, reason: RejectionReason);
}
export declare class MaxFilesError {
}
export declare type UploadZoneError = FileUploadError | FileRejectedError | MaxFilesError;
export declare class UploadZone extends React.Component<IProps, IState> {
    state: IState;
    fileIdSequence: number;
    filesUploading: IFile[];
    componentDidUpdate(oldProps: IProps, oldState: IState): void;
    getFiles(): IFile[];
    uploadNewFiles(files: IFile[]): void;
    sendRequest(file: IFile): IRequest;
    updateFile(id: number, updater: (f: IFile) => Partial<IFile>): void;
    onDrop: (acceptedFiles: File[], rejectedFiles: File[]) => void;
    removeFile: (fileId: number) => void;
    cancelUpload: (fileId: number) => void;
    onError(error: UploadZoneError): void;
    render(): JSX.Element;
}
