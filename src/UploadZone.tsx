import React from 'react';
import Dropzone, { DropzoneRenderArgs } from 'react-dropzone';

export interface IProps {
  accept?: string;
  preventDropOnDocument?: boolean;
  disabled?: boolean;

  maxFiles?: number;
  maxFileSize?: number;
  uploadFile: FileUploader;

  onChange?: (files: IFile[]) => void;
  onError?: (e: UploadZoneError) => void;
  onFilesUploaded?: (files: IFile[]) => void;
  onFilesUploading?: (added: IFile[], uploading: IFile[]) => void;

  children: (props: DropzoneRenderArgs & IUploadZoneRenderArgs) => JSX.Element;
}

export interface IRequest {
  abort: () => void;
}

export type FileUploader = (args: {
  file: IFile;
  onSuccess: (data: any) => void;
  onError: (error: string) => void;
  onProgress: (loaded: number, total: number) => void;
}) => IRequest;

export interface IUploadZoneRenderArgs {
  files: IFile[];
  removeFile: (id: number) => void;
  cancelUpload: (id: number) => void;
}

export interface IFile {
  id: number;
  nativeFile: File;
  request?: IRequest;
  progress?: number;
  response?: any;
  isUploadAttempted?: boolean;
  isUploading?: boolean;
  isUploaded?: boolean;
  error?: string;
}

export interface IState {
  files: IFile[];
}

// Error types
export enum RejectionReason {
  MIME_TYPE = 'MIME_TYPE',
  MAX_FILE_SIZE = 'MAX_FILE_SIZE'
}
export class FileUploadError {
  constructor(public file: IFile, public message: string) {}
}
export class FileRejectedError {
  constructor(public file: File, public reason: RejectionReason) {}
}
export class MaxFilesError {}

export type UploadZoneError =
  | FileUploadError
  | FileRejectedError
  | MaxFilesError;
// End Error types

function compareObjectArrays<T extends { id: number }>(
  oldFiles: T[],
  newFiles: T[]
) {
  const oldMap: { [k: number]: T } = {};
  const newMap: { [k: number]: T } = {};
  oldFiles.forEach((file) => (oldMap[file.id] = file));
  newFiles.forEach((file) => (newMap[file.id] = file));
  const added = newFiles.filter((file) => !oldMap[file.id]);
  const removed = oldFiles.filter((file) => !newMap[file.id]);
  const same = oldFiles.filter((file) => newMap[file.id]);
  return {
    added,
    removed,
    same,
    changed: added.length > 0 || removed.length > 0
  };
}

export class UploadZone extends React.Component<IProps, IState> {
  state: IState = {
    files: []
  };

  fileIdSequence: number = 0;
  filesUploading: IFile[] = [];

  componentDidUpdate(oldProps: IProps, oldState: IState): void {
    if (oldState.files !== this.state.files) {
      this.props.onChange && this.props.onChange(this.state.files);

      const filesUploading = this.state.files.filter((f) => f.isUploading);
      const changes = compareObjectArrays(this.filesUploading, filesUploading);
      if (changes.changed) {
        this.filesUploading = filesUploading;
        if (filesUploading.length === 0 && changes.removed.length > 0) {
          this.props.onFilesUploaded &&
            this.props.onFilesUploaded(this.state.files);
        }
        if (changes.added.length > 0) {
          this.props.onFilesUploading &&
            this.props.onFilesUploading(changes.added, filesUploading);
        }
      }

      this.uploadNewFiles(this.state.files);
    }
  }

  public getFiles() {
    return this.state.files;
  }

  uploadNewFiles(files: IFile[]): void {
    let isChanged = false;
    const newFiles: IFile[] = files.map((file: IFile) => {
      if (!file.isUploadAttempted) {
        isChanged = true;
        const request = this.sendRequest(file);
        return {
          ...file,
          request,
          isUploading: true,
          isUploadAttempted: true,
          progress: 0
        };
      } else {
        return file;
      }
    });

    if (isChanged) {
      this.setState({ files: newFiles });
    }
  }

  sendRequest(file: IFile): IRequest {
    return this.props.uploadFile({
      file,
      onSuccess: (data: any) => {
        this.updateFile(file.id, () => ({
          response: data,
          isUploading: false,
          isUploaded: true
        }));
      },
      onError: (error: string) => {
        this.updateFile(file.id, () => ({
          isUploading: false,
          error: error
        }));
        this.onError(new FileUploadError(file, error));
      },
      onProgress: (loaded: number, total: number) => {
        const progress = Math.round((loaded / total) * 100);
        this.updateFile(file.id, () => ({ progress }));
      }
    });
  }

  updateFile(id: number, updater: (f: IFile) => Partial<IFile>): void {
    this.setState((state) => {
      let isChanged = false;
      const files = state.files.map((f) => {
        if (f.id === id) {
          isChanged = true;
          return { ...f, ...updater(f) };
        } else {
          return f;
        }
      });

      if (!isChanged) {
        console.warn(
          `File with id=${id} not found. This may be due to the uploading request not properly aborted.`
        ); // `
      }

      return { files };
    });
  }

  onDrop = (acceptedFiles: File[], rejectedFiles: File[]) => {
    if (rejectedFiles && rejectedFiles.length) {
      rejectedFiles.forEach((file) => {
        this.onError(new FileRejectedError(file, RejectionReason.MIME_TYPE));
      });
    }

    if (
      this.props.maxFiles &&
      this.state.files.length + acceptedFiles.length > this.props.maxFiles
    ) {
      this.onError(new MaxFilesError());
      return;
    }

    const passed: IFile[] = [];
    acceptedFiles.forEach((file) => {
      if (this.props.maxFileSize && file.size > this.props.maxFileSize) {
        this.onError(
          new FileRejectedError(file, RejectionReason.MAX_FILE_SIZE)
        );
        return;
      }

      passed.push({
        id: this.fileIdSequence++,
        nativeFile: file
      });
    });

    this.setState({
      files: [...this.state.files, ...passed]
    });
  };

  removeFile = (fileId: number) => {
    this.setState({
      files: this.state.files.filter((f) => f.id !== fileId)
    });
  };

  cancelUpload = (fileId: number) => {
    const file = this.state.files.find((f) => f.id === fileId);
    if (file && file.request) {
      file.request.abort();
    }
    this.removeFile(fileId);
  };

  onError(error: UploadZoneError) {
    this.props.onError && this.props.onError(error);
  }

  render() {
    return (
      <Dropzone
        accept={this.props.accept}
        preventDropOnDocument={this.props.preventDropOnDocument}
        disabled={this.props.disabled}
        multiple={true}
        onDrop={this.onDrop}
      >
        {(dropzoneProps: DropzoneRenderArgs) => {
          return this.props.children({
            ...dropzoneProps,
            files: this.state.files,
            removeFile: this.removeFile,
            cancelUpload: this.cancelUpload
          });
        }}
      </Dropzone>
    );
  }
}
